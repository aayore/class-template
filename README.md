REPLACE ALL THIS WITH YOUR CONTENT

# Class Template

A template for technical classes by the Software Freedom School

This is a template project to help teachers "cover the bases"

In the same sense that the Time Pie helps the teacher to address all the learner's needs in terms of hearing, seeing, doing, and verbalizing, this will template will help the teacher (or materials author, but usually that's the same person) to not miss important things, like requirements.

files:

- CONTRIBUTING.md - Guidelines for those submitting MR's or pushes
- LICENSE.md - All SFS Materials including this template are CC BY SA
- REQUIREMENTS.md - Requirements of the participant learner
- LABS/ - Lesson folders

ideas:

- SFS-Method should be re-formatted to match this template.
- SOURCE - Address of the original source of the mats. i.e. where to look for updates
- Maintainers/Credits? - Maybe it's not necessary to name the maintainer/s, since each commit is signed? But, what about shallow-clones?
